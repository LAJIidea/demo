# Redis

Redis默认有16个数据库

切换数据库

```bash
127.0.0.1:6379> select 3 #切换数据库
set name king 	#新建一个key
get name	#获取key的value
keys * #查看所有的key
flushdb	#清空当前数据库
FLUSHALL #清空所有数据库
DBSIZE #查看当前库的大小
move name 1 #移动一个key到指定库
type name	#查看key是什么类型
```

Redis是单线程的，Redis是很快的，是基于内存操作的，CPU不是Rdis性能瓶颈，机器的内存和网络带宽是Redis的瓶颈

Redis是C语言写的，官方提供的数据为100000+QPS,完全不比MemerCache差

## 五大数据类型

### Redis-Key



### String

```bash
set key1 v1	#设置值
get key1 # 获取值
exists key1 # 是否存在这个key
append key1 "hello" #追加这个key的value
strlen key1 # 这个key的长度
set views 0
incr views #加1
decr views # 减1
incrby views 10 #自增，设置步长为10
getrange key1 0 3 # 获取子串，0-3(包括首尾)
setrange key1 1 xx # 从2开始替换，后面跟的value长度为多少，就替换多少个
set key3 60
setex key3 30 "hello" #将key3设置为hello,30秒后过期
setnx key4 40 # 不存在就新建key4,存在就新建失败
mset key1 v1 key2 v2 key3 v3 #批量新增
mget #批量读取 是一个原子性的操作，一起成功，要么一起失败
mset user:1:name zhangsan user:1:age 2 #设置user对象的name,age
getset #先get再set,如果不存在值返回null，否则返回当前的值
```

### List

所有的list命令都是L开头的，Redis不区分大小写

```bash
LPUSH list one # 将一个值放在列表头部
LPUSH list two	
LPUSH list three
LRANGE list 0 -1 # 获取list的值
RPUSH list four #将一个值放在列表尾部
Lpop list #将列表头部第一个元素移除
Rpop list #移除列表尾部第一个元素
lindex list 1 #通过下标获取list中的某一个值
Llen list # list的长度
Lrem list 1 one # 将list中的one移除1个
ltrim list 1 2 # 通过下标截取指定的长度
rpoplpush list myotherlist #移除列表最后一个元素，将它加到新表中
lset list 0 item #将list第0个元素替换成item
```

- 实际上是一个双向链表
- 消息队列  LPush Rpop,栈 LPUSH,LPOP 

### SET

```bash
sadd myset hello #添加一个值
smembers myset # 取出set的所有值
sismember hello #set中存在hello这个value就返回1否则返回0
srandmember myset 2 #随机抽选出指定个数的元素
spop myset #随机删除set中的元素
smove myset myset2 hello #将一个指定的值移动到另一个set中
sdiff 差集
sinter 交集
sunion 并集
```

set中的值不能重复

### Hash（map集合）

```bash
hset myhash field1 kingkiller #添加一个键值对
hget myhash field1 #根据key取出value
hgetall myhash #获取hash中的所有数据
hdel myhash field1 #删除指定的键值对
hlen myhash # 获取hash的长度
hexists myhash field1 #判断是否存在key
```

### Zset

比set多个socre字段

```bash
zadd salary 2500 kingkiller # 填加一个值
zadd salary 5000 zhangsan 6000 lisi #增加多个值
zrangebyscore salary -inf +inf #按score字段排序从真无穷到负无穷
```

## 事务

mysql事务要么成功，要么失效，原子性

Redis单条命令执行存在原子性，但是Redis事务不保证原子性

Redis事务的本质：一组命令的集合，一个事务的所有命令都会被序列化，在事务执行中，会安装顺序执行。（一次性，顺序性，排他性）

```

```

==Redis事务不保证原子性==

==Redis事务没有隔离级别的概念==

所有的命令在事务中，没有直接被执行，只有发起执行命令才会执行

Redis事务：

- 开启事务（）

- 命令入队（）

- 执行事务（）

  ```bash
  multi #开启事务
  
  #命令入队
  set k1 v1
  set k2 v2
  get k2
  exec #执行事务
  dicard #取消事务 一旦放弃所有事务，事务队列中的命令都不被执行
  ```

  事务队列中命令有错，事务中所有的命令都不被执行！

  事务队列中有运行时异常，事务中其他命令可以正常执行

  ## 监控

  ### 悲观锁

  
